import { pick } from "./modules/utils.js"

const NOTES = ["A3", "A2", "C4", "C3", "F3", "F2"]
export const NOTE = pick(NOTES)

const SPACING = ["7h", "8h", "9h", "10h"]
export const SPACE = pick(SPACING)

const RECORDINGS = ["birds.mp3", "stairs.mp3", "train.mp3"]
export const RECORDING = pick(RECORDINGS)

Tone.Transport.bpm.value = 60;
