export const reverb = new Tone.Reverb({
  decay : 30,
  wet : 0.5
})
