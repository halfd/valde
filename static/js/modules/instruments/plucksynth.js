export const plucksynth = new Tone.PluckSynth({
  volume : 3,
  attackNoise : 1,
  resonance : 0.9
})
