export const monosynth = new Tone.MonoSynth({
	volume: -9,
	envelope: {
		attack: 0.01,
	},
	filterEnvelope: {
		attack: 0.001,
		decay: 0.01,
		sustain: 0.9,
		baseFrequency: 1000,
		octaves: 1.6
	}
})
