from flask import Flask, render_template
from flask_socketio import SocketIO, emit

app = Flask(__name__)
socketio = SocketIO(app)

# Normal app routes
@app.route('/')
def index():
    return render_template('index.html')

@app.route('/spe')
def spe():
    return render_template('spe.html')

# Socket IO stuff
@socketio.on('action')
def action():
    socketio.emit('all_action')

@socketio.on('noise')
def noise():
    socketio.emit('all_noise')

@socketio.on('stop')
def stop():
    socketio.emit('all_stop')

if __name__ == '__main__':
    socketio.run(
        app,
        host='0.0.0.0',
        allow_unsafe_werkzeug=True
    )
